# asignacion
#1. ancho/2
#2. ancho/2.0
#3. alto/3
#4. 1 + 2 * 5
# autor: Danilo Delgado
# email: edwin.delgado@unl.edu.ec

ancho=17
alto=12.0

R1 = ancho/2
print(type(R1))
print(R1)


R2 = ancho/2.0
print(type(R2))
print(R2)


R3 = alto/3
print(type(R3))
print(R3)


R4 = 1+2*5
print(type(R4))
print(R4)